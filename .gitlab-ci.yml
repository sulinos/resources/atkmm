# 2022-05-11: ubuntu:latest = 22.04, ubuntu:rolling = 22.04, ubuntu:devel = 22.10
# See https://hub.docker.com/_/ubuntu
image: ubuntu:rolling

stages:
  - deps
  - build
  - deploy

variables:
  MESON_GCC_DEPS: g++
                  gettext
                  git
                  yelp-tools
                  gtk-doc-tools
                  python3-pygments
                  python3-setuptools
                  libglib2.0-dev
                  libatk1.0-dev
                  mm-common
                  libxml-libxml-perl
                  meson
                  ninja-build
                  glib-networking
  GIO_EXTRA_MODULES: "/usr/lib/x86_64-linux-gnu/gio/modules"
  GIT_SUBMODULE_STRATEGY: normal

.build_default:
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt update && apt -y upgrade && apt -y install $DEPENDENCIES

# Clear the cache manually.
clear_cache:
  stage: deps
  when: manual
  script:
    - echo Clearing the build-deps cache
  cache:
    key: build-deps
    paths:
      - libsigc/
      - glibmm/
    policy: push

build_deps:
  extends: .build_default
  stage: deps
  variables:
    DEPENDENCIES: $MESON_GCC_DEPS docbook5-xml docbook-xsl
  script:
    # Build and cache dependencies that can't be installed with apt.
    # Install to ./installdir, with the contents of the installed files
    # (notably .pc files) suited for installation to /usr.
    - export DESTDIR=`pwd`/installdir
    # Build libsigc++3
    - if test ! -d libsigc; then
    - git clone --branch 3.2.0 --depth 1 https://github.com/libsigcplusplus/libsigcplusplus.git libsigc
    - cd libsigc
    - mkdir _build && cd _build
    - meson --prefix=/usr --libdir=lib -Dvalidation=false -Dbuild-examples=false -Dbuildtype=release
    - meson compile
    - cd ../..
    - fi
    - ninja -C libsigc/_build install
    - cp -r installdir/usr /
    # Build glibmm-2.68
    - if test ! -d glibmm; then
    - git clone --branch 2.72.1 --depth 1 https://gitlab.gnome.org/GNOME/glibmm.git glibmm
    - cd glibmm
    - mkdir _build && cd _build
    - meson --prefix=/usr --libdir=lib -Dbuild-examples=false -Dbuildtype=release
    - meson compile
    - cd ../..
    - fi
    - ninja -C glibmm/_build install
  cache:
    key: build-deps
    paths:
      - libsigc/
      - glibmm/
    policy: pull-push
  # Transfer the installed part of the built dependencies to later stages
  # as artifacts. The cache does not always work.
  # https://gitlab.gnome.org/Infrastructure/Infrastructure/-/issues/775
  artifacts:
    paths:
      - installdir/
    expire_in: 1 day

release_gcc_build:
  extends: .build_default
  stage: build
  variables:
    DEPENDENCIES: $MESON_GCC_DEPS
  script:
    - cp -r installdir/usr /
    - mkdir _build && cd _build
    # -Ddebug=false + -Doptimization=3 correspond to -Dbuildtype=release
    - meson --prefix=/usr --libdir=lib -Ddebug=false -Doptimization=3 -Dwarnings=fatal -Dwarning_level=3 -Dwerror=true
    - meson compile
    - meson test
    - meson install
  artifacts:
    when: always
    paths:
      - _build/doc/reference

release_clang_build:
  extends: .build_default
  stage: build
  variables:
    DEPENDENCIES: $MESON_GCC_DEPS clang
  script:
    - cp -r installdir/usr /
    - mkdir _build && cd _build
    # -Ddebug=false + -Doptimization=3 correspond to -Dbuildtype=release
    - CC=clang CXX=clang++ meson --prefix=/usr --libdir=lib -Ddebug=false -Doptimization=3 -Dwarnings=fatal -Dwarning_level=3 -Dwerror=true
    - meson compile
    - meson test
    - meson install
  allow_failure: true
  artifacts:
    when: on_failure
    paths:
      - _build/meson-logs/testlog.txt
      - _build/meson-logs/meson-log.txt
    expire_in: 1 week

# Publish reference documentation at gnome.pages.gitlab.gnome.org/atkmm
pages:
  stage: deploy
  needs: [release_gcc_build]
  script:
    - mkdir public
    - mv _build/doc/reference/html/* public
  artifacts:
    paths:
      - public
  only:
    refs:
      - master
